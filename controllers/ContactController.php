<?php
namespace app\controllers;


use app\modules\Hotel\models\Hotel;
use app\modules\Questions\models\Questions;
use Yii;
use app\modules\Feedback\models\Feedback;

class ContactController extends FrontController
{



    public function actionIndex()
    {   $feedback = new Feedback();
        return $this->render('../themes/front/contacts', [
            'feedback' => $feedback,
        ]);
    }

    public function actionHelp()
    {   $questions = Questions::find()->with('lang')->all();
        $recomandations = Hotel::find()->with('lang')->limit(8)->all();
        return $this->render('../themes/front/help', [
            'recomandations' => $recomandations,
            'questions' => $questions,
        ]);
    }
}


