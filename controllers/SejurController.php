<?php
namespace app\controllers;


use app\modules\Parser\models\Tour;
use app\modules\Parser\models\TourLocation;
use app\modules\Parser\models\TourSearch;
use Yii;


class SejurController extends FrontController
{

    public function actionAvionSearch()
    {
        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search([Yii::$app->request->queryParams,'type' => 'Airplane']);

        return $this->render('../themes/front/avionSearch', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBusSearch()
    {
        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search([Yii::$app->request->queryParams,'type' => 'Bus']);//added aditional parameter type
        return $this->render('../themes/front/busSearch', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionSejurView($id)
    {   $sejur = Tour::find()->where(['ID'=>$id])->one();
        //$recomandations = Tour::find()->with(['tourLocations'])->where(['Location'=>$hotel->Location])->limit(8)->all();
        return $this->render('../themes/front/sejurView', [
            'sejur' => $sejur,

        ]);

    }
    public function actionAutoCompleteLocations($location){
        $query = TourLocation::find();
        $query -> andFilterWhere(['or',
            ['like','Country', $location],
            ['like','Location', $location],
        ]);

        $locations = $query->limit(20)->all();

        $result = [];
        foreach ($locations as $location)
        {
            $result[$location->Location] = [
            'id' => $location->ID,
            'label' => $location->Location,
        ];
            $result[$location->Country] = [
                'id' => $location->ID,
                'label' => $location->Country,
            ];
        }

        exit(json_encode($result));
    }
}


