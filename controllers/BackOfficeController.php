<?php
namespace app\controllers;

use Yii;
use app\controllers\BackendController;
use yii\web\NotFoundHttpException;
use app\modules\User\models\User;

class BackOfficeController extends BackendController
{
    
    public function init()
    {
        return $this->checkPermisions();
    }
    
    public function beforeAction($action)
    {
        $this->layout = 'backend';
        return parent::beforeAction($action);
    }

    public function actionError()
    {
        throw new NotFoundHttpException("Page not found");
    }
    
    public function checkPermisions()
    {
        if (!in_array(Yii::$app->user->identity->Type, [User::TypeAccountant, User::TypeSuperAdmin]))
        {
            return $this->redirect(['/user/login']);
        }
    }
    
}