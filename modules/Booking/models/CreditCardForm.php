<?php
namespace app\modules\Booking\models;

use Yii;
use yii\base\Model;

class CreditCardForm extends Model
{
    
    public $HolderName;
    public $Number;
    public $ExpirationDate;
    public $CVV;

    public function rules()
    {
        return [
            [['HolderName', 'Number', 'ExpirationDate', 'CVV'], 'required'],
            [['CVV'], 'number'],
            [['CVV'], 'string', 'min' => 3, 'max' => 4],
        ];
    }

    public function attributeLabels()
    {
        return [
            'HolderName' => Yii::t('app', 'Deținator a cardului'),
            'Number' => Yii::t('app', 'Numărul cardului'),
            'ExpirationDate' => Yii::t('app', 'Data de expirare'),
            'CVV' => Yii::t('app', 'CVV'),
        ];
    }
    
}