<?php

namespace app\modules\Booking\models;

use Yii;

/**
 * This is the model class for table "BookingInfo".
 *
 * @property integer $ID
 * @property integer $BookingID
 * @property string $Data
 */
class BookingInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BookingInfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BookingID'], 'integer'],
            [['Data'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'BookingID' => Yii::t('app', 'Booking ID'),
            'Data' => Yii::t('app', 'Data'),
        ];
    }
}
