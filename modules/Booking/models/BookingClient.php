<?php

namespace app\modules\Booking\models;

use Yii;

/**
 * This is the model class for table "BookingClient".
 *
 * @property integer $ID
 * @property integer $BookingID
 * @property string $FirstName
 * @property string $LastName
 * @property string $Email
 * @property string $Phone
 * @property string $City
 * @property string $Street
 * @property string $StreetNumber
 * @property string $Apartament
 * @property string $Zip
 */
class BookingClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BookingClient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BookingID', 'FirstName', 'LastName', 'Email', 'Phone', 'City', 'Street', 'StreetNumber', 'Apartament', 'Zip'], 'required'],
            [['FirstName', 'LastName', 'Email', 'Phone', 'City', 'Street', 'StreetNumber', 'Apartament', 'Zip'], 'string', 'max' => 255],
            ['BookingID', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'FirstName' => Yii::t('app', 'First Name'),
            'LastName' => Yii::t('app', 'Last Name'),
            'Email' => Yii::t('app', 'Email'),
            'Phone' => Yii::t('app', 'Phone'),
            'City' => Yii::t('app', 'City'),
            'Street' => Yii::t('app', 'Street'),
            'StreetNumber' => Yii::t('app', 'Street Number'),
            'Apartament' => Yii::t('app', 'Apartament'),
            'Zip' => Yii::t('app', 'Zip'),
        ];
    }
}
