<?php

namespace app\modules\Booking\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Booking\models\Booking;

/**
 * BookingSearch represents the model behind the search form about `app\modules\Booking\models\Booking`.
 */
class BookingSearch extends Booking
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'OperatorID', 'ExternalID'], 'integer'],
            [['RateKey', 'RateType', 'PaymentStatus', 'PaymentType', 'Type', 'Hash'], 'safe'],
            [['Amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Booking::find()->with(['bookingClient', 'operator']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'OperatorID' => $this->OperatorID,
            'ExternalID' => $this->ExternalID,
            'Amount' => $this->Amount,
        ]);

        $query->andFilterWhere(['like', 'RateKey', $this->RateKey])
            ->andFilterWhere(['like', 'RateType', $this->RateType])
            ->andFilterWhere(['like', 'PaymentStatus', $this->PaymentStatus])
            ->andFilterWhere(['like', 'PaymentType', $this->PaymentType])
            ->andFilterWhere(['like', 'Type', $this->Type])
            ->andFilterWhere(['like', 'Hash', $this->Hash]);

        return $dataProvider;
    }
}
