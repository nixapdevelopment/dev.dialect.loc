<?php

namespace app\modules\Booking\models;

use Yii;

/**
 * This is the model class for table "Booking".
 *
 * @property integer $ID
 * @property integer $OperatorID
 * @property integer $ExternalID
 * @property string $RateKey
 * @property string $RateType
 * @property string $Amount
 * @property string $PaymentStatus
 * @property string $PaymentType
 * @property string $Type
 * @property string $Hash
 * @property BookingClient $bookingClient
 */
class Booking extends \yii\db\ActiveRecord
{
    
    const PaymentStatusPending = 'Pending';
    const PaymentStatusPaid = 'Paid';
    const PaymentStatusCanceled = 'Canceled';
    
    const TypeHotel = 'Hotel';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OperatorID', 'RateKey', 'RateType', 'PaymentStatus', 'PaymentType', 'Type'], 'required'],
            [['OperatorID', 'ExternalID'], 'integer'],
            [['Amount'], 'number'],
            [['RateKey', 'Hash'], 'string', 'max' => 1000],
            [['RateType', 'PaymentStatus', 'PaymentType', 'Type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OperatorID' => Yii::t('app', 'Operator ID'),
            'ExternalID' => Yii::t('app', 'External ID'),
            'RateKey' => Yii::t('app', 'Rate Key'),
            'RateType' => Yii::t('app', 'Rate Type'),
            'Amount' => Yii::t('app', 'Amount'),
            'PaymentStatus' => Yii::t('app', 'Payment Status'),
            'PaymentType' => Yii::t('app', 'Payment Type'),
            'Type' => Yii::t('app', 'Type'),
        ];
    }
    
    public function getBookingClient()
    {
        return $this->hasOne(BookingClient::className(), ['BookingID' => 'ID']);
    }
    
    public function getOperator()
    {
        return $this->hasOne(\app\modules\Operator\Operator\models\Operator::className(), ['ID' => 'OperatorID']);
    }
    
    public function getInfo()
    {
        return $this->hasOne(BookingInfo::className(), ['BookingID' => 'ID']);
    }
    
}
