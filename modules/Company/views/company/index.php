<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Company\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Companies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Company'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'ID',
                'Denumire',
                'CodFiscal',
                'NrRegistrulComertului',
                'CodCAEN',
                // 'Judet',
                // 'Localitate',
                // 'Sector',
                // 'Strada',
                // 'Numar',
                // 'CodPostal',
                // 'Bloc',
                // 'Scara',
                // 'Etaj',
                // 'Apartament',
                // 'Telefon',
                // 'Email:email',
                // 'OperatiiInValuta',
                // 'MetodaDeIesireStocuri',
                // 'ModulDePlataTVA',
                // 'TVAColectataLaIncasare',
                // 'PersoanaJuridicaFaraScopLucrativ',
                // 'Microintreprindere',
                // 'CapitalSocial',
                // 'TipDeCalculAlDateiScadenteiInFacturi',

                [
                    'class' => 'app\components\GridView\ActionColumn'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
    
</div>
