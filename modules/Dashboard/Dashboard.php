<?php

namespace app\modules\Dashboard;

use app\components\Module\Module;

/**
 * dashboard module definition class
 */
class Dashboard extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Dashboard\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
