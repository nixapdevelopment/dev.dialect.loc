<?php

namespace app\modules\Dashboard\controllers;

use app\controllers\BackendController;

/**
 * Default controller for the `dashboard` module
 */
class DefaultController extends BackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
