<?php

namespace app\modules\OurInfo\models;

use Yii;

/**
 * This is the model class for table "OurInfoLang".
 *
 * @property integer $ID
 * @property integer $OurInfoID
 * @property string $Title
 * @property string $Content
 * @property string $ShortContent
 *
 * @property OurInfo $ourInfo
 */
class OurInfoLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OurInfoLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OurInfoID', 'Title', 'Content'], 'required'],
            [['OurInfoID'], 'integer'],
            [['Content', 'ShortContent'], 'string'],
            [['Title'], 'string', 'max' => 255],
            [['OurInfoID'], 'exist', 'skipOnError' => true, 'targetClass' => OurInfo::className(), 'targetAttribute' => ['OurInfoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OurInfoID' => Yii::t('app', 'Our Info ID'),
            'Title' => Yii::t('app', 'Title'),
            'Content' => Yii::t('app', 'Content'),
            'ShortContent' => Yii::t('app', 'Short Content'),
        ];
    }


}
