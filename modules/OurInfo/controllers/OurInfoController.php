<?php

namespace app\modules\OurInfo\controllers;

use Yii;
use app\modules\OurInfo\models\OurInfo;
use app\modules\OurInfo\models\OurInfoSearch;
use app\controllers\SiteBackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\modules\OurInfo\models\OurInfoImage;
use yii\imagine\Image;

/**
 * OurInfoController implements the CRUD actions for OurInfo model.
 */
class OurInfoController extends SiteBackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OurInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OurInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OurInfo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OurInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OurInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OurInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OurInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OurInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OurInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OurInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $image = UploadedFile::getInstanceByName('Image');
        if($image){

                $file = md5(microtime(true)) . '.' . $image->extension;
                $thumb = 'thumb_' . $file;

                if ($image->saveAs(Yii::getAlias("@webroot/uploads/ourinfo/$file")))
                {   Image::thumbnail(Yii::getAlias("@webroot/uploads/ourinfo/$file"), 200, 200)->save(Yii::getAlias("@webroot/uploads/ourinfo/$thumb"));


                    $imageModel = new OurInfoImage([
                        'OurInfoID' => $model->ID,
                        'Thumb' => $thumb,
                        'Image' => $file,
                    ]);
                    $imageModel->save();
                }
            }
    }

    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');

        OurInfoImage::findOne($key)->delete();
        return '{}';
    }

}
