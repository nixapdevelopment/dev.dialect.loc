<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Clienti\models\Clienti */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Clienti',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clientis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="clienti-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
