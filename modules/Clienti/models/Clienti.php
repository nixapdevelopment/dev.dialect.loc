<?php

namespace app\modules\Clienti\models;

use Yii;
use app\modules\Agenti\models\Agenti;
use app\modules\Judet\models\Judet;

/**
 * This is the model class for table "Clienti".
 *
 * @property integer $ID
 * @property string $Denumire
 * @property string $CodFiscal
 * @property string $ContAnalitic
 * @property integer $Judet
 * @property string $Adresa
 * @property string $ContBancar
 * @property string $Banca
 * @property string $Telefon
 * @property string $Email
 * @property string $NrRegistrulComertului
 * @property string $Delegat
 * @property string $CISerie
 * @property string $CINumar
 * @property string $EliberatDe
 * @property string $MijloculDeTransport
 * @property integer $Agent
 * @property string $Reducere
 *
 * @property Agenti $agent
 * @property Judet $judet
 */
class Clienti extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Clienti';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Denumire', 'CodFiscal', 'ContAnalitic'], 'required'],
            [['Judet', 'Agent'], 'integer'],
            [['Reducere'], 'number'],
            [['Denumire', 'CodFiscal', 'ContAnalitic', 'Adresa', 'ContBancar', 'Banca', 'Telefon', 'Email', 'NrRegistrulComertului', 'Delegat', 'CISerie', 'CINumar', 'EliberatDe', 'MijloculDeTransport'], 'string', 'max' => 255],
            [['Agent'], 'exist', 'skipOnError' => true, 'targetClass' => Agenti::className(), 'targetAttribute' => ['Agent' => 'ID']],
            [['Judet'], 'exist', 'skipOnError' => true, 'targetClass' => Judet::className(), 'targetAttribute' => ['Judet' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Denumire' => Yii::t('app', 'Denumire'),
            'CodFiscal' => Yii::t('app', 'Cod Fiscal'),
            'ContAnalitic' => Yii::t('app', 'Cont Analitic'),
            'Judet' => Yii::t('app', 'Judet'),
            'Adresa' => Yii::t('app', 'Adresa'),
            'ContBancar' => Yii::t('app', 'Cont Bancar'),
            'Banca' => Yii::t('app', 'Banca'),
            'Telefon' => Yii::t('app', 'Telefon'),
            'Email' => Yii::t('app', 'Email'),
            'NrRegistrulComertului' => Yii::t('app', 'Nr. Registrul Comertului'),
            'Delegat' => Yii::t('app', 'Delegat'),
            'CISerie' => Yii::t('app', 'CI serie'),
            'CINumar' => Yii::t('app', 'CI numar'),
            'EliberatDe' => Yii::t('app', 'Eliberat De'),
            'MijloculDeTransport' => Yii::t('app', 'Mijlocul De Transport'),
            'Agent' => Yii::t('app', 'Agent'),
            'Reducere' => Yii::t('app', 'Reducere'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Agenti::className(), ['ID' => 'Agent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudet()
    {
        return $this->hasOne(Judet::className(), ['ID' => 'Judet']);
    }
}
