<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\Booking\models\Booking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booking-form">

    <a class="btn btn-primary" href="<?= Url::to('/backoffice/booking/booking/index') ?>"><i class="fa fa-chevron-left"></i> Inapoi</a>
    
    <?php $form = ActiveForm::begin(); ?>
    
    <h2 class="title">
        <button class="btn btn-danger btn-lg"><i class="fa fa-times" aria-hidden="true"></i> Cancelation</button>
        &nbsp;&nbsp;&nbsp;
        <button class="btn btn-primary"><i class="fa fa-times" aria-hidden="true"></i> Genereaza vaucer</button>
    </h2>
    <div class="row">
        <div class="col-md-6">
            <h2 class="title">Booking Info</h2>
            <div class="providers">
                <h3>Operator: <?= $model->operator->Name ?></h3>
                <h3>Hotel: <?= $cacheInfo['Name'] . ' ' . $cacheInfo['Stars'] ?>*</h3>
                <h3>Address: <?= $location->lang->Name ?>, <?= $cacheInfo['Address'] ?></h3>
                <h3>Data: <?= $info['CheckIn'] ?> - <?= $info['Nights'] ?> nopti</h3>
                <h3>Info: <?= reset($info['Adults']) ?> adults, <?= reset($info['Childs']) ?> childs, <?= $info['Rooms'] ?> rooms</h3>
                <h3>Amount: <?= $model->Amount ?> EUR</h3>
                <h3>Rate Type: <?= $model->RateType ?></h3>
                <h3>Payment Status: <?= $model->PaymentStatus ?></h3>
                
            </div>
        </div>
        <div class="col-md-6">
            <h2 class="title">Client Info</h2>
            <div class="providers">
                <h3>Client: <?= $model->bookingClient->FirstName ?> <?= $model->bookingClient->LastName ?></h3>
                <h3>Email: <?= $model->bookingClient->Email ?></h3>
                <h3>Telefon: <?= $model->bookingClient->Phone ?></h3>
                <h3>Adresa: <?= $model->bookingClient->City ?>, <?= $model->bookingClient->Street ?> <?= $model->bookingClient->StreetNumber ?>, ap. <?= $model->bookingClient->Apartament ?></h3>
                <h3>Cod postal: <?= $model->bookingClient->Zip ?></h3>
            </div>
        </div>
    </div>
    <br />

    <?php ActiveForm::end(); ?>

</div>
