<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Booking\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bookings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Booking'), ['/backoffice/hotel/hotel-search'], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <?php Pjax::begin([
        'timeout' => 5000,
    ]); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'ID',
                'operator.Name',
                //'ExternalID',
                //'RateKey:ntext',
                [
                    'attribute' => 'RateType',
                    'filter' => Html::activeDropDownList($searchModel, 'RateType', [
                        '' => '-',
                        'BOOKABLE' => 'BOOKABLE',
                        'RESERVABLE' => 'RESERVABLE',
                    ], [
                        'class' => 'form-control'
                    ]),
                ],
                'Amount',
                [
                    'label' => 'Client',
                    'value' => function($model)
                    {
                        return $model->bookingClient->FirstName . ' ' . $model->bookingClient->LastName;
                    }
                ],
                [
                    'attribute' => 'PaymentStatus',
                    'filter' => Html::activeDropDownList($searchModel, 'PaymentStatus', [
                        '' => '-',
                        'Paid' => 'Paid',
                        'Pending' => 'Pending',
                        'Canceled' => 'Canceled',
                    ], [
                        'class' => 'form-control'
                    ]),
                ],
                'PaymentType',
                'Type',
                
                // 'Hash',

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
