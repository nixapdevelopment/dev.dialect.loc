<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\PunctDeLucru\models\PunctDeLucru */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Punct De Lucru',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Punct De Lucrus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="punct-de-lucru-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
