<?php

namespace app\modules\PunctDeLucru\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "PunctDeLucru".
 *
 * @property integer $ID
 * @property string $Denumire
 *
 * @property Salariati[] $salariati
 */
class PunctDeLucru extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PunctDeLucru';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Denumire'], 'required'],
            [['Denumire'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Denumire' => Yii::t('app', 'Denumire'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalariati()
    {
        return $this->hasMany(Salariati::className(), ['PunctDeLucru' => 'ID']);
    }
    
    public static function getList($addEmpty = false)
    {
        $result = $addEmpty ? ['' => $addEmpty] : [];
        
        $judete = ArrayHelper::map(self::find()->orderBy('ID')->all(), 'ID', 'Denumire');
        
        return $result + $judete;
    }
    
}
