<?php

/**
 * Created by PhpStorm.
 * User: dennix
 * Date: 27.07.17
 * Time: 18:33
 */
namespace app\modules\Comment\widgets\CommentWidget;


use yii\base\Widget;
use yii\data\ActiveDataProvider;
use app\modules\Comment\models\Comment;

class CommentWidget extends Widget
{

    public $postID = null;

    public function init()
    {
        parent::init();

    }


    public function run()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Comment::find()->where(['PostID' => $this->postID,'Status' => Comment::StatusActive])
                ->orderBy(['CreatedAt'=>SORT_DESC]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

            $newComment = new Comment();
        if ($newComment->load(\Yii::$app->request->post())&& \Yii::$app->request->isPost) {

            $comment = \Yii::$app->request->post('Comment');
            $newComment->Content = $comment['Content'];
            $newComment->Status = 'active';
            $newComment->PostID = $this->postID;

            if($newComment->save()) {
                //echo 'saved';
                \Yii::$app->session->setFlash('success');
            }else{
                echo 'error';
            }
        }

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'newComment' => $newComment,
            ]);


    }

}