<?php

namespace app\modules\Parser\controllers;

use yii\web\Controller;
use yii\httpclient\Client;

class MelitourController extends Controller
{
    
    public function actionIndex()
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl('http://api-test.meli-tours.com/')
            ->setData([
                'requestXML' => "
                    <HotelTypeListRQ>
                        <RequestorID>
                            <UserName>xmldialect</UserName>
                            <Password>xmldialect</Password>
                        </RequestorID>
                        <Language>RO</Language>
                    </HotelTypeListRQ>
                ",
            ])
            ->send();
        
        if ($response->isOk) 
        {
            echo '<pre>';
            print_r($response->data);
            exit;
        }
    }
    
}