<?php

namespace app\modules\Parser\controllers;

use yii\web\Controller;
use yii\httpclient\Client;


class ParalelaController extends Controller
{
    
    public $username = 'XML_DIALECT'; 
    
    public $password = 'fcv34t453reg';

    public function actionIndex()
    {
        $endPoint = 'http://rezervari.paralela45.ro/server_xml/server.php';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($endPoint)
            ->setHeaders([
                'Content-Type' => 'text/xml'
            ])
            ->setData("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request RequestType=\"getCountryRequest\"><AuditInfo><RequestId>001</RequestId><RequestUser>$this->username</RequestUser><RequestPass>$this->password</RequestPass><RequestTime>$this->time</RequestTime><RequestLang>RO</RequestLang></AuditInfo><RequestDetails><getCountryRequest/></RequestDetails></Request>")
            ->send();
        
        echo '<pre>';
        print_r($response);
            exit;
        
        if ($response->isOk)
        {
            echo '<pre>';
            var_dump($response->data);
            exit;
        }
    }
    
    public function getRequestID()
    {
        return time();
    }
    
    public function getTime()
    {
        return date('Y-m-d\TH:i:s');
    }
    
}
