<?php

namespace app\modules\Location;

use app\components\Module\SiteModule;

/**
 * Location module definition class
 */
class Location extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Location\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
