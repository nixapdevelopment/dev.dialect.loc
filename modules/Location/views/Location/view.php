<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ArrayDataProvider;
use app\components\GridView\GridView;
/* @var $this yii\web\View */
/* @var $model app\modules\Location\models\Location */

$this->title = $model->lang->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'parentLang.Name',
                'label' => 'Tara',
            ],
            [
                'attribute'=>'lang.Name',
                'label' => 'Regiune',
            ],
            [
                'attribute'=>'lang.Description',
                'label' => 'Descriere',
            ],

            'Latitude',
            'Longitude',


        ],
    ]) ?>

</div>
    <span>Hotels:</span>
<?php
$provider = new ArrayDataProvider([
    'allModels' => $model->allHotels,

]);
echo GridView::widget([
    'dataProvider' => $provider,
    'columns' => [

        [
            'attribute'=>'location.parentLang.Name',
            'label' => 'Country / Region',
        ],
        [
            'attribute'=>'location.lang.Name',
            'label' => 'Location',
        ],
        [
            'attribute'=>'lang.Name',
            'label' => 'Hotel',
        ],
        [
            'attribute'=>'lang.Description',
            'label' => 'Descriere',
        ],
        ['class' => 'app\components\GridView\ActionColumn',

            'urlCreator' => function ($action, $model, $key, $index, $ac) {
                $url = ["/site/hotel/hotel/$action",'id' => $model->ID];
                return $url;
            }
            ],

    ],
]);