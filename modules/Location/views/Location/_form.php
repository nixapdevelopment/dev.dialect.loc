<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Location\models\Location;
use yii\bootstrap\Tabs;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\Location\models\Location */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, "Type")->dropDownList( $model->typeList ,['id'=>'country-type']) ?>

        <?php
        $regionSelected =[];
        $locationSelected = [];
        if ($model->CountryID){
        $locationSelected = [$model->CountryID => $model->country->lang->Name];
        }
        if ($model->ParentID ){
            if($model->Type == 'Region'){$regionSelected = [];}else{
        $regionSelected = [$model->ParentID => $model->parent->parentLang->Name];
            }
        }
        ?>

    <div class="row">
        <div class="col-md-4">
            <?php
            echo $form->field($model, 'CountryID')->widget(DepDrop::classname(), [
                'options'=>['id'=>'country-id'],
                'data' => $locationSelected,
                'pluginOptions'=>[
                    'depends'=>['country-type'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/site/location/location/locations-type-country']),
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
            echo $form->field($model, 'ParentID')->label('Region')->widget(DepDrop::classname(), [
                'options'=>['id'=>'region-id'],
                'data' => $regionSelected,
                'pluginOptions'=>[
                    'depends'=>[ 'country-id','country-type'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/site/location/location/locations-type-region']),
                ]
            ]);
            ?>
        </div>
    </div>
    <?php
    $items = [];
    foreach ($model->langs as $langID => $langModel)
    {
        $items[] = [
            'label' => strtoupper($langID),
            'content' => $this->render('_lang_form',[
                'form' => $form,
                'langModel' => $langModel,
            ]),

        ];
    }

    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>

    <?= $form->field($model, "Latitude")->textInput() ?>
    <?= $form->field($model, "Longitude")->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
