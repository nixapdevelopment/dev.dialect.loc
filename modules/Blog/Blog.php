<?php

namespace app\modules\Blog;

use app\components\Module\SiteModule;

/**
 * blog module definition class
 */
class Blog extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Blog\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
