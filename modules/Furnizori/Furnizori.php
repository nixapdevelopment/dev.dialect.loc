<?php

namespace app\modules\Furnizori;

/**
 * Furnizori module definition class
 */
class Furnizori extends \app\components\Module\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Furnizori\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
