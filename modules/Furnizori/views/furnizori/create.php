<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Furnizori\models\Furnizori */

$this->title = Yii::t('app', 'Create Furnizori');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Furnizoris'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="furnizori-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
