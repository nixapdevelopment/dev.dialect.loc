<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Furnizori\models\Furnizori */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Furnizori',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Furnizoris'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="furnizori-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
