<?php

namespace app\modules\Agenti\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Agenti".
 *
 * @property integer $ID
 * @property string $Denumire
 * @property string $Telefon
 * @property string $CISerie
 * @property string $CINumar
 */
class Agenti extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Agenti';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Denumire', 'Telefon'], 'required'],
            [['Denumire', 'Telefon'], 'string', 'max' => 255],
            [['CISerie', 'CINumar'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Denumire' => Yii::t('app', 'Denumire'),
            'Telefon' => Yii::t('app', 'Telefon'),
            'CISerie' => Yii::t('app', 'CI serie'),
            'CINumar' => Yii::t('app', 'CI numar'),
        ];
    }
    
    public static function getList($addEmpty = false)
    {
        $result = $addEmpty ? ['' => $addEmpty] : [];
        
        $judete = ArrayHelper::map(self::find()->orderBy('Denumire')->all(), 'ID', 'Denumire');
        
        return $result + $judete;
    }
    
}
