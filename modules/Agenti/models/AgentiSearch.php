<?php

namespace app\modules\Agenti\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Agenti\models\Agenti;

/**
 * AgentiSearch represents the model behind the search form about `app\modules\Agenti\models\Agenti`.
 */
class AgentiSearch extends Agenti
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Denumire', 'Telefon', 'CISerie', 'CINumar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agenti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
        ]);

        $query->andFilterWhere(['like', 'Denumire', $this->Denumire])
            ->andFilterWhere(['like', 'Telefon', $this->Telefon])
            ->andFilterWhere(['like', 'CISerie', $this->CISerie])
            ->andFilterWhere(['like', 'CINumar', $this->CINumar]);

        return $dataProvider;
    }
}
