<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Agenti\models\Agenti */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Agenti',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agentis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agenti-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
