<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\Agenti\models\AgentiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Agenti');
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="min-height: 500px;" class="agenti-index">
    
    <br />
    
    <?php Pjax::begin([
        'id' => 'list-pjax',
    ]); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'Denumire',
                'Telefon',
                'CISerie',
                'CINumar',

                [
                    'class' => 'app\components\GridView\ActionColumn',
                    'header' => Html::a(Yii::t('app', 'Adauga'), '#edit-modal', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['create'])]),
                    'buttons' => [
                        'update' => function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#edit-modal', [
                                'title' => 'Modifica', 
                                'data-toggle' => 'modal', 
                                'data-backdrop' => false, 
                                'data-remote' => $url,
                            ]);
                        },
                        'delete' => function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::current(['delete' => $model->ID]), [
                                'title' => 'Sterge',
                                'onclick' => 'return confirm("' . Yii::t('app', 'Sterge?') . '");',
                                'data-pjax' => '0'
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
    
    <?php Modal::begin([
        'id' => 'edit-modal',
        'size' => Modal::SIZE_LARGE,
        'header' => Html::tag('h4', Yii::t('app', 'Editarea agent')),
    ]) ?>
    <?php Modal::end() ?>
    
    <?php $this->registerJs("
        $(document).on('click','[data-remote]', function(e) {
            e.preventDefault();
            $('#edit-modal .modal-body').load($(this).data('remote'));
        });
        $(document).on('pjax:success', '#edit-form-wrap', function() {
            $.pjax.reload({container: '#list-pjax'});
            $('#edit-modal').modal('hide');
        });
    "); ?>

</div>
