<?php

namespace app\modules\TipuriDeArticole;

/**
 * tipuriDeArticole module definition class
 */
class TipuriDeArticole extends \app\components\Module\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\TipuriDeArticole\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
