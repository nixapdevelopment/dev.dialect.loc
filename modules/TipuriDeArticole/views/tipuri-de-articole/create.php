<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\TipuriDeArticole\models\TipuriDeArticole */

$this->title = Yii::t('app', 'Create Tipuri De Articole');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipuri De Articoles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipuri-de-articole-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
