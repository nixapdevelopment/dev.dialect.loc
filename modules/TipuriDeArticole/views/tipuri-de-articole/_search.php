<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\TipuriDeArticole\models\TipuriDeArticoleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipuri-de-articole-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Denumire') ?>

    <?= $form->field($model, 'ContArticol') ?>

    <?= $form->field($model, 'Diferente') ?>

    <?= $form->field($model, 'TVANeexigibila') ?>

    <?php // echo $form->field($model, 'Cheltuieli') ?>

    <?php // echo $form->field($model, 'Venituri') ?>

    <?php // echo $form->field($model, 'Aprovizionabil') ?>

    <?php // echo $form->field($model, 'Consumabil') ?>

    <?php // echo $form->field($model, 'Vandabil') ?>

    <?php // echo $form->field($model, 'Produs') ?>

    <?php // echo $form->field($model, 'Nestocat') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
