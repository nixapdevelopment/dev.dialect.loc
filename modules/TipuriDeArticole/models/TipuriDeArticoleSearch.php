<?php

namespace app\modules\TipuriDeArticole\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\TipuriDeArticole\models\TipuriDeArticole;

/**
 * TipuriDeArticoleSearch represents the model behind the search form about `app\modules\TipuriDeArticole\models\TipuriDeArticole`.
 */
class TipuriDeArticoleSearch extends TipuriDeArticole
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Aprovizionabil', 'Consumabil', 'Vandabil', 'Produs', 'Nestocat'], 'integer'],
            [['Denumire', 'ContArticol', 'Diferente', 'TVANeexigibila', 'Cheltuieli', 'Venituri'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TipuriDeArticole::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Aprovizionabil' => $this->Aprovizionabil,
            'Consumabil' => $this->Consumabil,
            'Vandabil' => $this->Vandabil,
            'Produs' => $this->Produs,
            'Nestocat' => $this->Nestocat,
        ]);

        $query->andFilterWhere(['like', 'Denumire', $this->Denumire])
            ->andFilterWhere(['like', 'ContArticol', $this->ContArticol])
            ->andFilterWhere(['like', 'Diferente', $this->Diferente])
            ->andFilterWhere(['like', 'TVANeexigibila', $this->TVANeexigibila])
            ->andFilterWhere(['like', 'Cheltuieli', $this->Cheltuieli])
            ->andFilterWhere(['like', 'Venituri', $this->Venituri]);

        return $dataProvider;
    }
}
