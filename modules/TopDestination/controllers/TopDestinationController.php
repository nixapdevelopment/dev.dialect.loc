<?php

namespace app\modules\TopDestination\controllers;

use Yii;
use app\modules\TopDestination\models\TopDestination;
use app\modules\TopDestination\models\TopDestinationSearch;
use app\controllers\SiteBackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * TopDestinationController implements the CRUD actions for TopDestination model.
 */
class TopDestinationController extends SiteBackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TopDestination models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TopDestinationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TopDestination model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TopDestination model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TopDestination();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->Image = $this->saveImage($model);

            if ($model->save()){

            return $this->redirect(['view', 'id' => $model->ID]);
        } }
            return $this->render('create', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing TopDestination model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->Image = $this->saveImage($model);
            if ($model->save()){
            return $this->redirect(['view', 'id' => $model->ID]);
        } }
            return $this->render('update', [
                'model' => $model,
            ]);

    }

    /**
     * Deletes an existing TopDestination model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TopDestination model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TopDestination the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TopDestination::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $image = UploadedFile::getInstanceByName('Image');
        if($image){


                $file = md5(microtime(true)) . '.' . $image->extension;


                if ($image->saveAs(Yii::getAlias("@webroot/uploads/topDestination/$file")))
                {
                     return $file;
                }
        }
        return $model->Image;
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        $model = TopDestination::findOne($key);
        $model->Image = '*';
        $model->save();
        return '{}';
    }
}
