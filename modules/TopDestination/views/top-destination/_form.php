<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\TopDestination\models\TopDestination */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="top-destination-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
    <?php

    $items = [];
    foreach ($model->langs as $langID => $langModel)
    {
        $items[] = [
            'label' => strtoupper($langID),
            'content' => $this->render('_desc_form',[
                'form' => $form,
                'langModel' => $langModel,
                'model' => $model,
            ]),
        ];
    }
    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>

    <?= $form->field($model, 'Link')->textInput(['maxlength' => true]) ?>
    <?php
    $initialPreview = [];
    $initialPreviewConfig = [];
    if ($model->imagePath){
    $initialPreview[] = Html::img($model->imagePath,['width' => 200]);
    }
    $initialPreviewConfig[] = [
        'url' => \yii\helpers\Url::to(['/site/top-destination/top-destination/image-delete']),
        'key' => $model->ID,
    ];


    ?>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo FileInput::widget([
                'name' => 'Image',
                'options'=>['accept'=>'image/*','multiple' => true],
                'pluginOptions' => [
                    'overwriteInitial'=>false,
                    'maxFileSize'=>2800,
                    'fileActionSettings' => [
                        'fileActionSettings' => [
                            'showZoom' => false,
                            'showDelete' => true,
                        ],
                    ],
                    'browseClass' => 'btn btn-success',
                    'uploadClass' => 'btn btn-info',
                    'removeClass' => 'btn btn-danger',
                    'showRemove' => false,
                    'showUpload' => false,
                    'initialPreview' => $initialPreview,
                    'initialPreviewConfig' => $initialPreviewConfig,
                ],

            ]);

            ?>

        </div></div>
    <div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
