<?php

namespace app\modules\TopDestination\models;

use Yii;

/**
 * This is the model class for table "TopDestinationLang".
 *
 * @property integer $ID
 * @property integer $TopDestinationID
 * @property string $LangID
 * @property string $Title
 *
 * @property TopDestination $topDestination
 */
class TopDestinationLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TopDestinationLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TopDestinationID', 'LangID', 'Title'], 'required'],
            [['TopDestinationID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['TopDestinationID'], 'exist', 'skipOnError' => true, 'targetClass' => TopDestination::className(), 'targetAttribute' => ['TopDestinationID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TopDestinationID' => Yii::t('app', 'Top Destination ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopDestination()
    {
        return $this->hasOne(TopDestination::className(), ['ID' => 'TopDestinationID']);
    }
}
