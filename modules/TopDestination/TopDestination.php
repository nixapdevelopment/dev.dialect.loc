<?php

namespace app\modules\TopDestination;

use app\components\Module\SiteModule;
/**
 * TopDestination module definition class
 */
class TopDestination extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\TopDestination\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
