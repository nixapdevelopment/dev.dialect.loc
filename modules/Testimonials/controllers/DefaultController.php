<?php

namespace app\modules\Testimonials\controllers;

use app\controllers\SiteBackendController;

/**
 * Default controller for the `Testimonials` module
 */
class DefaultController extends SiteBackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
