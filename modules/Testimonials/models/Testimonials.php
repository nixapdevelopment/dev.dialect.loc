<?php

namespace app\modules\Testimonials\models;

use Yii;
use yii\db\ActiveRecord;
use yii\caching\TagDependency;



/**
 * This is the model class for table "Testimonials".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Function
 * @property string $Content
 */
class Testimonials extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Testimonials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Function', 'Content'], 'required'],
            [['Content'], 'string'],
            [['Name', 'Function'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Name' => Yii::t('app', 'Name'),
            'Function' => Yii::t('app', 'Function'),
            'Content' => Yii::t('app', 'Content'),
        ];
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
    }
    
}
