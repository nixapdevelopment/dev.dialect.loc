<?php

namespace app\modules\Category;

use app\components\Module\Module;

/**
 * category module definition class
 */
class Category extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Category\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
