<?php
namespace app\modules\Articole\controllers;

use app\controllers\BackendController;

class ArticoleController extends BackendController
{
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
}