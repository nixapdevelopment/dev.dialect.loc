<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Location\models\Location;
use yii\bootstrap\Tabs;
use kartik\file\FileInput;
use yii\widgets\Pjax;
use kartik\rating\StarRating;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\Hotel */
/* @var $form yii\widgets\ActiveForm */
Pjax::begin();
?>

<div class="hotel-form">

    <?php $form = ActiveForm::begin([
            'options' => [
                    'enctype' => 'multipart/form-data',
            ]
    ]);

    ?>


    <?php echo $form->field($model, 'Stars')->widget(StarRating::classname(), [
        'pluginOptions' => [
            'value' => 3,
            'stars' => 5,
            'min' => 0,
            'max' => 5,
            'step' => 1,
            'theme' => 'krajee-svg',
            'filledStar' => '<span class="krajee-icon krajee-icon-star"></span>',
            'emptyStar' => '<span class="krajee-icon krajee-icon-star"></span>'

        ]]);


    $items[0] = [
    'label' => 'Descriere',
    'content' => $this->render('_lang_form',[
        'form' => $form,
        'model' => $model,

        ])];
        $items[1] = [
        'label' => 'Imagini',
        'content' => $this->render('_image_form',[
            'form' => $form,
            'model' => $model,


        ])];

    echo '<br>';
    echo Tabs::widget([
    'items' => $items,
    ]);

    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end();
    Pjax::end();
    ?>
    </div>
</div>