<?php
use yii\bootstrap\Tabs;
use app\modules\Location\models\Location;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\bootstrap\Html;

$regionSelected =[];
$locationSelected = [];
if ($model->RegionID){
    $regionSelected = [$model->RegionID => $model->region->lang->Name];
}
if ($model->LocationID){
    $locationSelected = [$model->LocationID => $model->location->lang->Name];
}
?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'CountryID')->dropDownList( Location::getList('Select',Location::TypeCountry),['id'=>'country-id'] ) ?>
        </div>
        <div class="col-md-4">
            <?php
           echo $form->field($model, 'RegionID')->widget(DepDrop::classname(), [
                'options'=>['id'=>'region-id'],
                'data' => $regionSelected,
                'pluginOptions'=>[
                    'depends'=>['country-id'],
                    'placeholder'=>'Select...',
                    'value' => $model->RegionID,
                    'url'=>Url::to(['/site/location/location/regions']),
                    'params' => ['model_id1'],
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
           echo $form->field($model, 'LocationID')->widget(DepDrop::classname(), [
                'options'=>['id'=>'location-id'],
                'data' => $locationSelected,
                'pluginOptions'=>[
                    'depends'=>[ 'region-id'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/site/location/location/locations']),
                    'params' => ['model_id2'],
                ]
            ]);
            ?>
        </div>
    </div>
            <?php
            $items = [];
            foreach ($model->langs as $langID => $langModel)
            {
                $items[] = [
                    'label' => strtoupper($langID),
                    'content' => $this->render('_desc_form',[
                        'form' => $form,
                        'langModel' => $langModel,
                        'model' => $model,
                    ]),

                ];
            }
            echo '<br>';
            echo Tabs::widget([
                'items' => $items,
            ]);
            ?>

