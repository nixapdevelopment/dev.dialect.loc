<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\Hotel */

$this->title = $model->lang->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hotels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
   <?php
   if ($model->mainImage) {
       echo Html::img($model->mainImage->imagePath, ['width' => 400, 'class' => 'centered']);
   }

   echo \kartik\rating\StarRating::widget([

       'name' => 'stars',
       'value' => $model->Stars,
       'pluginOptions' => [
           'readonly' => true,
           'showClear' => false,
           'showCaption' => false,
       ]
   ])
   ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [



            [
                'attribute' => 'country.lang.Name',
                'label' => 'Tara',
            ],
            [
                'attribute'=>'region.lang.Name',
                'label' => 'Regiune',
            ],
            [
                'attribute'=>'city.lang.Name',
                'label' => 'Oras',
            ],
            [
                'attribute'=>'lang.Name',
                'label' => 'Hotel',
            ],
            [
                'attribute'=>'lang.Description',
                'label' => 'Description',
                'format' => 'raw',
            ],
            [
                'attribute'=>'lang.Address',
                'label' => 'Adresa',
            ],

        ],

    ]); ?>

</div>

