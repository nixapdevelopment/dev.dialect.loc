<?php

namespace app\modules\Hotel\controllers;

use app\controllers\BackendController;
use Yii;
use app\modules\Hotel\models\Hotel;
use app\modules\Hotel\models\HotelSearch;
use app\modules\Hotel\controllers\DefaultController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\modules\Hotel\models\HotelImage;
use yii\imagine\Image;

/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class HotelController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hotel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HotelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hotel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hotel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Hotel();

        //$this->saveImage($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Hotel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hotel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function saveImage($model){

        $images = UploadedFile::getInstancesByName('Images');
        if($images){

            foreach ($images as $image)
            {
                $file = md5(microtime(true)) . '.' . $image->extension;
                $thumb = 'thumb_' . $file;

                if ($image->saveAs(Yii::getAlias("@webroot/uploads/hotel/$file")))
                {   Image::thumbnail(Yii::getAlias("@webroot/uploads/hotel/$file"), 200, 200)->save(Yii::getAlias("@webroot/uploads/hotel/$thumb"));


                    $imageModel = new HotelImage([
                        'HotelID' => $model->ID,
                        'Thumb' => $thumb,
                        'Image' => $file,
                        'IsMain' => 1,
                    ]);
                    $imageModel->save();
                }
            }
        }
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        HotelImage::findOne($key)->delete();
        return '{}';
    }
    public function actionSetMainImage(){
        $key = Yii::$app->request->post('id');
        $imgModel = HotelImage::findOne($key);

        HotelImage::updateAll(['IsMain'=> 0],['HotelID'=>$imgModel->HotelID]);

            $imgModel->IsMain = 1;
            $rs = $imgModel->save();





        return "$rs";
    }

}
