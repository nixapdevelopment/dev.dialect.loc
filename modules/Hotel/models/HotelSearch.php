<?php

namespace app\modules\Hotel\models;

use app\modules\Location\models\Location;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Hotel\models\Hotel;

/**
 * HotelSearch represents the model behind the search form about `app\modules\Hotel\models\Hotel`.
 */
class HotelSearch extends Hotel
{
    /**
     * @inheritdoc
     */
    public $countryID = NULL;
    public $regionID = NULL;
    public $cityID = NULL;
    public $starsFrom = NULL;
    public $starsTo = NULL;
    public $Name = NULL;


    public function rules()
    {
        return [
            [['ID', 'LocationID','starsFrom','starsTo'], 'integer'],
            [['regionID','countryID','cityID','Name'],'safe'],


        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Hotel::find()->joinWith(['location.lang'])->with('country.lang','region.lang','city.lang');

        $dataProvider = new ActiveDataProvider([

            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        if ($this->starsFrom ){

            $query->andFilterWhere(['>=', 'Stars', $this->starsFrom]);

        }
        if ($this->starsTo){

            $query->andFilterWhere(['<=', 'Stars', $this->starsTo]);
        }
        if ($this->countryID){

            $query->andFilterWhere(['like', 'Hotel.CountryID', $this->countryID])->with('lang');

        }
        if ($this->regionID && $this->countryID){

            $query->andFilterWhere(['like', 'Hotel.RegionID', $this->regionID])->with('lang');
        }
        if ($this->cityID && $this->regionID && $this->countryID){

            $query->andFilterWhere(['like', 'Hotel.LocationID', $this->cityID])->with('lang');

        }

        if ($this->Name || Yii::$app->request->get('sort')){

            $query ->joinWith(['lang']);
            $query->andFilterWhere(['like','HotelLang.Name', $this->Name]);
        }
        $dataProvider->sort->attributes['Name'] = [

            'asc'  => ['HotelLang.Name' => SORT_ASC],
            'desc' => ['HotelLang.Name' => SORT_DESC],
        ];

        return $dataProvider;
    }
}
