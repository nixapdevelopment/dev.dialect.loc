<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\Salariati\models\Salariati */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salariatis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salariati-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'Nume',
            'Prenume',
            'PunctDeLucru',
            'Functie',
            'DataAngajarii',
            'Tip',
            'FunctieBaza',
            'NormaPeZi',
            'OrePeLuna',
            'CASIndividuala',
            'CASAngajator',
            'ContrSomajIndividuala',
            'ContrSomajAngajator',
            'ContrFGCS',
            'ContrAccMunca',
            'ZileCOAn',
            'TipSalariu',
            'Avans',
            'SalariuBrut',
            'SalariuOrar',
            'CNP',
            'Judet',
            'Localitate',
            'Strada',
            'Numar',
            'CodPostal',
            'Bloc',
            'Scara',
            'Etaj',
            'Apartament',
            'Sector',
            'Telefon',
            'NrContract',
            'DataContract',
            'CasaDeSanatate',
            'Impozitat',
            'Pensionar',
            'FaraContribCCI',
            'FaraContribSanatateAngajator',
            'FaraContribSanatateSalariat',
            'TipPlata',
            'CISerieNumar',
            'CIEliberatDe',
            'CIEliberatData',
            'Email:email',
            'NormaPeZiSpecific',
            'OrePeLunaSpecific',
        ],
    ]) ?>

</div>
