<?php

namespace app\modules\Judet\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Judet".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $ShortName
 */
class Judet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Judet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'ShortName'], 'required'],
            [['Name'], 'string', 'max' => 255],
            [['ShortName'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Name' => Yii::t('app', 'Denumire'),
            'ShortName' => Yii::t('app', 'Short Name'),
        ];
    }
    
    public static function getList($addEmpty = false)
    {
        $result = $addEmpty ? ['' => $addEmpty] : [];
        
        $judete = ArrayHelper::map(self::find()->orderBy('Name')->all(), 'ID', 'Name');
        
        return $result + $judete;
    }
    
}
