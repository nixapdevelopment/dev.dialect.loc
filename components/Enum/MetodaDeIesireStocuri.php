<?php
namespace app\components\Enum;

use Yii;

class MetodaDeIesireStocuri extends Enum
{
    
    const FIFO = 'FIFO';
    const LIFO = 'LIFO';
    const CMP = 'CMP';
    const Manual = 'Manual';
    const InventarIntermitent = 'InventarIntermitent';

    public static function items()
    {
        return [
            self::FIFO => Yii::t('app', 'FIFO'),
            self::LIFO => Yii::t('app', 'LIFO'),
            self::CMP => Yii::t('app', 'Cost mediu ponderat'),
            self::Manual => Yii::t('app', 'Manual'),
            self::InventarIntermitent => Yii::t('app', 'Inventar intermitent'),
        ];
    }
    
}
