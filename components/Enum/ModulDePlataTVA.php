<?php
namespace app\components\Enum;

use Yii;

class ModulDePlataTVA extends Enum
{
    
    const Lunar = 'Lunar';
    const Trimestrial = 'Trimestrial';
    const Neplatitor = 'Neplatitor';
    
    public static function items()
    {
        return [
            self::Lunar => Yii::t('app', 'Lunar'),
            self::Trimestrial => Yii::t('app', 'Trimestrial'),
            self::Neplatitor => Yii::t('app', 'Neplatitor'),
        ];
    }
    
}