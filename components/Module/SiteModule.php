<?php

namespace app\components\Module;

use Yii;


class SiteModule extends yii\base\Module
{
    
    public function init()
    {
        parent::init();
        
        $this->layoutPath = Yii::getAlias('@app/views/themes/site/layouts');
    }
    
}
