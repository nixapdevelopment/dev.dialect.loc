<?php
namespace app\components\DatePicker;

use kartik\date\DatePicker as KDatePicker;

class DatePicker extends KDatePicker
{
    
    public function init()
    {
        parent::init();
        
        $this->type = self::TYPE_INPUT;
        $this->pluginOptions = [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy'
        ];
    }
    
}