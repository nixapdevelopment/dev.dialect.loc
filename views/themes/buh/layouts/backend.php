<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\views\themes\buh\assets\BuhAsset;
use kartik\growl\Growl;

$bundle = BuhAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=0" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- Redirect to http://browsehappy.com/ on IE 8-  -->
    <!--[if lte IE 8]>
    <style type="text/css">
        body{display:none!important;}
    </style>
    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/" />
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>
    
<?php 
    if (Yii::$app->session->getFlash('success'))
    {
        echo Growl::widget([
            'type' => Growl::TYPE_SUCCESS,
            'icon' => 'glyphicon glyphicon-ok-sign',
            'title' => 'Success',
            'showSeparator' => true,
            'body' => Yii::$app->session->getFlash('success')
        ]);
    } 
?>

    <nav class="nav" id="offcanvas1">
        <a href="#" class="js-toggle-offcanvas offcanvas-button button">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </a>
        <a href="#">
            <img src="<?= $bundle->baseUrl ?>/images/white-logo.png" alt="Logo">
        </a>
        <?= Menu::widget([
            'items' => [
                [
                    'label' => 'Rezervări',
                    'url' => '#',
                    'items' => [
                         ['label' => 'Hoteluri', 'url' => ['/backoffice/hotel/hotel-search']],
                         ['label' => 'Sejur Avion', 'url' => ['/articole/articole']],
                         ['label' => 'Sejur Autocar', 'url' => ['/furnizori/furnizori']],
                    ],
                ],
                [
                    'label' => 'Lista rezervări',
                    'url' => '#',
                    'items' => [
                         ['label' => 'Lista rezervări', 'url' => ['/backoffice/booking/booking']],
                    ],
                ],
                [
                    'label' => 'Creare rezervare',
                    'url' => '#',
                ],
                [
                    'label' => 'Contabilitate',
                    'url' => '#',
                    'items' => [
                         ['label' => 'Companii', 'url' => ['/company/company']],
                         ['label' => 'Articole', 'url' => ['/articole/articole']],
                         ['label' => 'Furnizori', 'url' => ['/furnizori/furnizori']],
                         ['label' => 'Clienti', 'url' => ['/clienti/clienti']],
                         ['label' => 'Agenti', 'url' => ['/agenti/agenti']],
                         ['label' => 'Salariati', 'url' => ['/salariati/salariati']],
                    ],
                ],
            ],
            'activateParents' => true,
            'options' => ['id' => 'slider-menu'],
            'submenuTemplate' => '<ul role="menu" class="sub-menu">{items}</ul>'."\n",
            'linkTemplate' => '<a href="{url}"><i class="fa fa-chevron-right"></i>{label}</a>'
        ]) ?>
    </nav>


    <div class="header">
        <div class="name-page pull-left">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <div class="profile-box pull-right">
            <div class="notification pull-left">
                <a href="#">
                    <span class="number-notification">
                    8
                </span>
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="511.626px" height="511.626px" viewBox="0 0 511.626 511.626" style="enable-background:new 0 0 511.626 511.626;"
                 xml:space="preserve">
                <path d="M500.922,193.257c-7.139-7.135-15.753-10.706-25.845-10.706h0.007V72.917c0-9.9-3.62-18.464-10.848-25.697
                    c-7.231-7.233-15.808-10.85-25.7-10.85c-87.745,73.089-173.018,109.636-255.815,109.636H45.682
                    c-12.562,0-23.318,4.471-32.264,13.418C4.471,168.373,0,179.129,0,191.688v54.816c0,12.563,4.464,23.319,13.412,32.264
                    c8.945,8.952,19.701,13.418,32.264,13.418h34.833c-3.235,10.284-5.33,20.804-6.28,31.549c-0.949,10.756-1.283,20.126-0.999,28.124
                    c0.284,7.994,1.714,17.604,4.283,28.835c2.568,11.229,4.759,19.698,6.567,25.406c1.807,5.715,4.853,14.705,9.135,26.98
                    c4.281,12.278,7.089,20.604,8.42,24.981c8.754,7.994,21.316,13.278,37.685,15.845c16.374,2.563,32.408,1.475,48.111-3.285
                    c15.702-4.757,26.314-12.662,31.833-23.695c-7.232-5.712-13.084-10.517-17.556-14.421c-4.474-3.901-9.041-8.654-13.706-14.271
                    c-4.665-5.612-7.849-10.849-9.563-15.701c-1.712-4.853-2.284-10.372-1.712-16.562c0.57-6.188,2.76-12.423,6.567-18.699
                    c-7.232-7.426-11.043-16.228-11.421-26.412c-0.378-10.178,2.572-19.746,8.852-28.691c6.279-8.945,14.943-15.229,25.981-18.843
                    c75.187,6.28,152.462,42.442,231.833,108.493c9.893,0,18.469-3.617,25.693-10.852c7.234-7.228,10.848-15.797,10.848-25.693V255.642
                    c10.096,0,18.709-3.567,25.845-10.706c7.132-7.139,10.704-15.752,10.704-25.837C511.626,209.01,508.054,200.396,500.922,193.257z
                     M438.537,354.997c-74.426-56.907-147.51-89.366-219.267-97.356v-77.09c71.183-7.804,144.277-40.446,219.267-97.927V354.997z"/>
                </svg>
                </a>
            </div>
            <div class="img-profile pull-left">
                <img src="<?= $bundle->baseUrl ?>/images/profile-photo.png" alt="Profil">
            </div>
            <div class="dropdown setting pull-right">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Contul meu
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="container-fluid">
        <?= $content ?>
    </div>
    
    <div class="footer">
        <div class="name-page">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'options' => ['class' => 'breadcrumb'],
            ]) ?>
        </div>
    </div>

    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
    