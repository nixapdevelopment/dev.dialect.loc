<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$menuItems = Yii::$app->user->isGuest ? [['label' => 'Login', 'url' => ['/user/login']]] : [
    ['label' => 'Hotels', 'url' => ['/site/hotel']],
    ['label' => 'Locatii', 'url' => ['/site/location']],
    ['label' => 'Slider', 'url' => ['/site/slider/default/update', 'id' => 1]],
    ['label' => 'Testimonials', 'url' => ['/site/testimonials']],
    ['label' => 'News', 'url' => ['/site/blog/post']],
    ['label' => 'Our info', 'url' => ['/site/our-info']],
    ['label' => 'Top destinations', 'url' => ['/site/top-destination']],
    ['label' => 'Logout (' . Yii::$app->user->identity->UserName .  ')', 'url' => ['/user/logout']],
];

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php 
    if (Yii::$app->session->getFlash('success'))
    {
        echo Growl::widget([
            'type' => Growl::TYPE_SUCCESS,
            'icon' => 'glyphicon glyphicon-ok-sign',
            'title' => 'Success',
            'showSeparator' => true,
            'body' => Yii::$app->session->getFlash('success')
        ]);
    } 
?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
