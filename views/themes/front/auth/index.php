<?php
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

?>

<section class="auth">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="text">
                    Client nou: Inregistrare
                </div>
                <form action="#" method="post">
                    <div class="hidden-content">
                        <div class="contact-form">
                            <div class="row">
                                <?php Pjax::begin([
                                    'enablePushState' => false,
                                    'enableReplaceState'=>false,

                                ])?>
                                <?php $form = ActiveForm::begin([
                                    'action' => ['/site/feedback/feedback/new-feedback'],
                                    'method' => 'post',
                                    'id'=> 'feedback',
                                    'fieldConfig' =>[
                                        'options' => [
                                            'tag' => false,
                                        ]
                                    ],
                                    'options' => [

                                        'class' => 'feedback-form',
                                        'data-pjax' => true,
                                    ]
                                ]); ?>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <?= $form->field($feedback, 'Nume',['template' => "{input}"])->input('text',[
                                            'placeholder' => 'Nume',
                                            'class' => '',
                                            'id' => 'inp1',
                                        ])->label(false); ?>
                                    </label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <?= $form->field($feedback, 'Prenume',['template' => "{input}"])->input('text',[
                                            'placeholder' => 'Prenume',
                                            'class' => '',
                                            'id' => 'inp4',
                                        ])->label(false); ?>
                                    </label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <?= $form->field($feedback, 'Email',['template' => "{input}"])->input('text',[
                                            'placeholder' => 'Email',
                                            'class' => '',
                                            'id' => 'inp3'
                                        ])->label(false); ?>
                                    </label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <input type="text" placeholder="Numar telefon">
                                    </label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <input type="text" placeholder="Parola">
                                    </label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <input type="text" placeholder="Confirma parola">
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <div class="need-to-complit text-center">
                                        *Toate campurile sunt obligatorii
                                    </div>
                                </div>
                            </div>



                            <?php ActiveForm::end(); ?>
                            <?php Pjax::end()?>
                        </div>
                    </div>
                    <div>
                        <button type="button" class="show-content-form">
                            Inregistreaza-te
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <div class="contact-form mt40">
                    <div class="text text-center">
                        Contul tau
                    </div>
                    <div class="row">
                        <?php Pjax::begin([
                            'enablePushState' => false,
                            'enableReplaceState'=>false,

                        ])?>
                        <?php $form = ActiveForm::begin([
                            'action' => ['/site/feedback/feedback/new-feedback'],
                            'method' => 'post',
                            'id'=> 'feedback',
                            'fieldConfig' =>[
                                'options' => [
                                    'tag' => false,
                                ]
                            ],
                            'options' => [

                                'class' => 'feedback-form',
                                'data-pjax' => true,
                            ]
                        ]); ?>

                        <div class="col-md-12 text-center">
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <?= $form->field($feedback, 'Email',['template' => "{input}"])->input('text',[
                                    'placeholder' => 'Email',
                                    'class' => '',
                                    'id' => 'inp3'
                                ])->label(false); ?>
                            </label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <input type="text" placeholder="Parola">
                            </label>
                        </div>
                        <div class="col-md-12">
                            <div class="text-center">
                                <?= Html::submitButton(Yii::t('app', 'Intra'), ['class' => 'submit-btn btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>



                    <?php ActiveForm::end(); ?>
                    <?php Pjax::end()?>
                </div>
            </div>
        </div>
    </div>
</section>
