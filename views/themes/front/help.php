<?php

use app\views\themes\front\assets\FrontAsset;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


$bundle = FrontAsset::register($this);

?>

<section class="name-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="title">
                    Intrebari frecvente
                </div>
            </div>
            <div class="col-md-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html">
                            ACASA
                        </a>
                    </li>
                    <li class="breadcrumb-item active">
                        Intrebari frecvente
                    </li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="info-for-user">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="recent-questions">
                    <?php
                    foreach ($questions as $index => $question){
                        $index= $index+1;
                        ?>


                    <div class="questions border-questions">
                        <?=$index.'. '. $question->lang->Question?>
                    </div>
                    <div class="answer">
                        <?= $question->lang->Content?>
                    </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
