<?php

use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;

Pjax::begin();
echo Html::tag('h2','Regions:');
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_region',
    'pager' => [
        'prevPageLabel' => 'previous',
        'nextPageLabel' => 'next',
        'maxButtonCount' => 5,
    ],

]);

Pjax::end();